import renpy
from renpy import ast
import random

def rng_config():
    return ("Randomize node chain", "The node chain have been randomized!")

def rng_execute():
    
    labels = {}
    blacklist = ["jz_randomizer_vars", "start", "splashscreen", "nameentry", "nameentryx", "nameentry2", "colormenu1", "colormenu2", "colormenu3", "colormenu4", "detected", "colorend", "skipintro"]
    
    for key, value in renpy.game.script.namemap.items():
        if isinstance(key, (str, unicode)):
            if not key.startswith("_") and not key.startswith("AWSW") and not key.endswith("check") and key not in blacklist:
                labels[key] = value
    
    node_list = []
    
    def browse_if(if_node):
        for entry in if_node.entries:
            if entry[1] is None:
                continue
            for n in entry[1]:
                if n is None or isinstance(n, ast.Label):
                    break
                if not isinstance(n, ast.Translate):
                    node_list.append(n)
                if isinstance(n, ast.If):
                    browse_if(n)
                elif isinstance(n, ast.Menu):
                    browse_menu(n)
    
    def browse_menu(menu_node):
        for item in menu_node.items:
            if item[2] is None:
                continue
            for n in item[2]:
                if n is None or isinstance(n, ast.Label):
                    break
                if not isinstance(n, ast.Translate):
                    node_list.append(n)
                if isinstance(n, ast.If):
                    browse_if(n)
                elif isinstance(n, ast.Menu):
                    browse_menu(n)
    
    for label in labels.values():
        node = label.next
        while node is not None and not isinstance(node, ast.Label):
            if not isinstance(node, ast.Translate):
                node_list.append(node)
            if isinstance(node, ast.If):
                browse_if(node)
            elif isinstance(node, ast.Menu):
                browse_menu(node)
            node = node.next
    
    random.shuffle(node_list)
    
    for i in range(len(node_list) // 6):
        node_list[i].chain(node_list[-1-i])
