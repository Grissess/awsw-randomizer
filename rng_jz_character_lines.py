import renpy
from renpy import ast
import random

def rng_config():
    return ("Randomize character lines", "Lines of all characters have been randomized!")

def rng_execute():
    character_lines = {}
    language_says = set((tl.block[0] for tl in renpy.game.script.translator.language_translates.values()))
        
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Say):
            if node.what is None or node.what.strip() == "" or node in language_says:
                continue
            if node.who not in character_lines:
                character_lines[node.who] = [node.what]
            else:
                character_lines[node.who].append(node.what)
    
    for ch, lines in character_lines.items():
        random.shuffle(lines)
    
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Say):
            if node.what is None or node.what.strip() == "" or node in language_says:
                continue
            node.what = character_lines[node.who].pop()
