import importlib
import os
import renpy.parser as parser
import renpy.ast as ast
from renpy import display 
from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod

def compile_to(code, screen):
    compiled = parser.parse("FNDummy", code)
    for node in compiled:
        if isinstance(node, ast.Init):
            for child in node.block[0].screen.children:
                modast.get_slscreen(screen).children.append(child)

@loadable_mod
class AWSWMod(Mod): 
    def mod_info(self):
        return ("AwSW Randomizer", "0.1.0", "Jakzie")
    
    def mod_load(self):
        mod_path = modinfo.get_mod_path("AwSW Randomizer")
        randomizers = []
        
        for python_file in os.listdir(mod_path):
            if os.path.isdir(os.path.join(mod_path, python_file)) or not python_file.startswith("rng_") or not python_file.endswith(".py"):
                continue
            randomizers.append(importlib.import_module(self.__module__ + "." + python_file[:-3]))
        
        tocompile = """
        screen dummy:
            imagebutton auto "ui/random_button_%s.png" action [Show("preferencesbg"), Show('jz_randomizer_menu'), Play("audio", "se/sounds/open.wav")] hovered Play("audio", "se/sounds/select.ogg") xalign 0.97 yalign 0.955
            """

        compile_to(tocompile, "main_menu")
        
        rng_menu_base = """
        screen dummy:
            frame id "jz_randomizer_menu" at popup:
                style "smallwindow"
        
                vbox xalign 0.5 ypos 0.15:
                    vbox xalign 0.5 yalign 0.5:{}
        
                imagebutton:
                    idle "image/ui/close_idle.png"
                    hover "image/ui/close_hover.png"
                    action [Hide("jz_randomizer_menu"), Hide("preferencesbg", transition=dissolve), ToggleVariable('navmenuopen', False), Play("audio", "se/sounds/close.ogg")]
                    hovered Play("audio", "se/sounds/select.ogg")
                    style "smallwindowclose"
                    at nav_button 
        """
        
        rng_button_base = """
        
                        textbutton "{}":
                            action [Show("jz_randomizer_menu_exec", transition=Dissolve(1.0)), SetVariable("jz_randomizer_rng_id", {}), Function(jz_randomizer_run), Play("audio", "se/sounds/open.ogg"), Hide("jz_randomizer_menu")]
                            hovered Play("audio", "se/sounds/select.ogg")
                            style "menubutton2"
        """
        
        rng_strings = []
        
        for i, randomizer in enumerate(randomizers):
            button_text = randomizer.rng_config()[0]
            rng_strings.append(rng_button_base.format(button_text, i))
        
        rng_finished_menu = rng_menu_base.format("\n".join(rng_strings))
        compile_to(rng_finished_menu, "jz_randomizer_menu")
        
        modast.set_renpy_global("jz_randomizer_rng_id", -1)
        
        def jz_randomizer_run():
            randomizer = randomizers[modast.get_renpy_global("jz_randomizer_rng_id")]
            randomizer.rng_execute()
            randomizer_msg = randomizer.rng_config()[1]
            modast.set_renpy_global("jz_randomizer_rng_label", randomizer_msg)
            print("[AwSW Randomizer]: " + randomizer_msg)
        
        modast.set_renpy_global("jz_randomizer_run", jz_randomizer_run)
        
        modast.call_hook(modast.find_label("nameentry"), modast.find_label("jz_randomizer_vars"))
        
    
    def mod_complete(self):
        pass
