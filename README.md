# AWSW-Randomizer

AwSW Randomizer is a mod for the game Angels with Scaly Wings which adds a new menu allowing you to randomize various aspects of the game. Chaos guaranteed.

## Contributing

If you want to add your own randomizer, create a file `rng_yourname_randomizername.py`. In the file there have to be these two functions:

```python
def rng_config():
    return ("text of the button", "text displayed after randomizer finishes")

def rng_execute():
    # Put the randomization code here
```
