import random
import renpy

def mapping_shuffle(d):
    vals = list(d.values())
    random.shuffle(vals)
    for k, v in zip(d.keys(), vals):
        d[k] = v

def rng_config():
    return ('Randomize all named images', 'All named images randomized!')

def rng_execute():
    mapping_shuffle(renpy.display.image.images)
