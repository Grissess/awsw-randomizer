import random
import renpy

def rng_config():
    return ('Randomize sounds', 'Sounds randomized!')

def rng_execute():
    extensions = set(('.ogg', '.wav', '.mp3', '.opus'))
    all_sounds = [
            filename for dirname, filename in renpy.loader.listdirfiles()
            if any(filename.lower().endswith(ext) for ext in extensions)
    ]

    old_play = renpy.audio.renpysound.play
    def play(channel, file, name, paused=False, fadein=0, tight=False, start=0, end=0):
        fi = renpy.loader.load(random.choice(all_sounds))
        old_play(
                channel = channel,
                file = fi,
                name = name,
                paused = paused,
                fadein = fadein,
                tight = tight,
                start = start,
                end = end,
        )
    renpy.audio.renpysound.play = play
