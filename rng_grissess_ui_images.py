import random, inspect
import renpy

class Namespace(object):
    'An object with a freely-assignable __dict__, nothing more.'
    def has(self, f):
        setattr(self, f.__name__, f)
        return f

# XXX This needs to be here because it's a global name in the function code we
# steal
Color = renpy.color.Color

def rng_config():
    return ('Randomize UI (and dynamic) image loading', 'Image loading randomized!')

def rng_execute():
    # XXX Evil bytecode hacking demands that we don't close over any variables
    # defined in this function, so--instead--we'll put them in a location that
    # is known to be globally-accessible. Ren'Py is an arbitrary choice; if
    # anyone can think of a better place, feel free.
    ns = Namespace()
    renpy.randomizer = ns  # Use this to reference ns in interior functions

    # Don't do this more than once
    if hasattr(renpy.easy.displayable, 'rand_injected'):
        return

    # Steal known-working copies of these functions
    @ns.has
    def real_displayable(d, scope = None):
        pass
    @ns.has
    def real_displayable_or_none(d, scope = None, dynamic = True):
        pass

    real_displayable.__code__ = renpy.easy.displayable.__code__
    real_displayable_or_none.__code__ = renpy.easy.displayable_or_none.__code__

    # Our randomizing processor
    image_extensions = set(('.png', '.jpg'))
    ns.all_images = [
            filename for dirname, filename in renpy.loader.listdirfiles()
            if any(filename.lower().endswith(ext) for ext in image_extensions)
    ]

    # XXX Should we make this pure in its input? Via hash() or somesuch?
    @ns.has
    def get_rand_image(d):
        return renpy.display.im.Image(random.choice(renpy.randomizer.all_images))

    # Blacklist a few files from getting processed, since we depend on them
    # functioning
    ns.postproc_blacklist = set(('transform.py', 'layout.py'))

    # Fallback if we're in early load and draw isn't ready yet
    ns.fallback = renpy.display.imagelike.Solid('#f0f')

    @ns.has
    def postproc(d):
        caller = inspect.currentframe().f_back.f_back  # "up two"
        if any(caller.f_code.co_filename.endswith(fn) for fn in renpy.randomizer.postproc_blacklist):
            return d
        if not isinstance(d, renpy.display.im.ImageBase):
            return d  # Not ours to play with
        if renpy.display.draw is None:
            return renpy.randomizer.fallback  # Still loading
        sz = renpy.display.im.cache.get(d).get_size()
        return renpy.display.transform.Transform(
                child = renpy.randomizer.get_rand_image(d),
                size = sz,
        )

    # Functions to be injected
    def rand_displayable(d, scope = None):
        return renpy.randomizer.postproc(renpy.randomizer.real_displayable(d, scope))
    def rand_displayable_or_none(d, scope = None, dynamic = True):
        return renpy.randomizer.postproc(renpy.randomizer.real_displayable_or_none(d, scope))

    renpy.easy.displayable.__code__ = rand_displayable.__code__
    renpy.easy.displayable_or_none.__code__ = rand_displayable_or_none.__code__
    renpy.easy.displayable.rand_injected = True

    # Refresh the slscreen cache while we're at it
    renpy.display.screen.prepared = False
    renpy.display.screen.prepare_screens()
