import renpy
from renpy import ast
import random

def rng_config():
    return ("Randomize choice menus", "The choice menus have been randomized!")

def rng_execute():
    blacklist = ["English {image=lang-en.png}", "English {image=lang-en2.png}", "{color=#0000ff}B{/color}lue", "{color=#00ffff}C{/color}yan", "{color=#ffd700}G{/color}old", "{color=#808000}O{/color}live", "No"]
        
    menu_items = []
        
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Menu):
            if node.items[0][0] in blacklist:
                continue
            for item in node.items:
                menu_items.append((item[0], item[2]))
        
    random.shuffle(menu_items)
        
    index = 0
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Menu):
            if node.items[0][0] in blacklist:
                continue
            for i, item in enumerate(node.items):
                node.items[i] = (menu_items[index][0], item[1], menu_items[index][1])
                index += 1
