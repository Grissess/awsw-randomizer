import renpy
from renpy import ast
import random

def rng_config():
    return ("Randomize character expressions", "Character expresions have been randomized!")

def rng_execute():
    
    image_expressions = {}
    full_image_expressions = {}
    for character in renpy.python.store_dicts["store"].values():
        if isinstance(character, renpy.character.ADVCharacter) and character.image_tag is not None:
            image_expressions[character.image_tag] = renpy.display.image.get_available_image_attributes(character.image_tag)[1:]
            full_image_expressions[character.image_tag] = []
    
    for name, d in renpy.display.image.images.items():
        if name[0] in full_image_expressions:
            full_image_expressions[name[0]].append(name)
    
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Say):
            if node.what is None or node.what.strip() == "" or not node.attributes or node.who is None or ast.eval_who(node.who).image_tag is None or node.who == "Z":
                continue
            
            character_expressions = renpy.display.image.get_available_image_attributes(ast.eval_who(node.who).image_tag)
            node.attributes = character_expressions[random.randint(0, len(character_expressions) - 1)]
        
        if isinstance(node, ast.Show):
            if node.imspec[0][0] not in full_image_expressions:
                continue
            
            temp_imspec = list(node.imspec)
            expression = temp_imspec[0][0]
            temp_imspec[0] = full_image_expressions[expression][random.randint(0, len(full_image_expressions[expression]) - 1)]
            node.imspec = tuple(temp_imspec)